
public class Aufgabe2 {

	public static void main(String[] args) {
		String a  = "0!";
		String b  = "1!";
		String c  = "2!";
		String d  = "3!";
		String e  = "4!";
		String f  = "5!";
		String ab = "";
		String bb = " 1";
		String cb = " 1 * 2";
		String db = " 1 * 2 * 3"; 
		String eb = " 1 * 2 * 3 * 4";
		String fb = " 1 * 2 * 3 * 4 * 5";
		String ac = "1";
		String bc = "1";
		String cc = "2";
		String dc = "6";
		String ec = "24"; 
		String fc = "120";
				
		System.out.printf("-----------AUFGABE 2----------%n");
		System.out.printf("                              %n");   //Leer Zeile
		System.out.printf("%-5s=%-19s=%4s%n" , a , ab , ac  );
		System.out.printf("%-5s=%-19s=%4s%n" , b , bb , bc  );
		System.out.printf("%-5s=%-19s=%4s%n" , c , cb , cc  );
		System.out.printf("%-5s=%-19s=%4s%n" , d , db , dc  );
		System.out.printf("%-5s=%-19s=%4s%n" , e , eb , ec  );
		System.out.printf("%-5s=%-19s=%4s%n" , f , fb , fc  );
}

	}


