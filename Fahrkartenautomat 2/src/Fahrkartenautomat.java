import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {
		
		//unendlichSchleife (!!!!ZUSATZ!!!!)
		//unendlichSchleife (!!!!ZUSATZ!!!!)
		//unendlichSchleife (!!!!ZUSATZ!!!!)
		int repeater = 1;
		
		do{
			double Berechnung1 = fahrkartenbestellungErfassen("Anzahl Fahrkarten: ");	
			double Berechnung2 = zuZahlenderBetrag("Noch zu zahlen: ", Berechnung1);
			fahrkartenAusgeben();
			rueckgeldAusgeben(Berechnung2);
			
			System.out.println("\n\n");
			for (int i = 0; i < 25; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");
			
        }while(repeater < 2);
	    	
	   		
	}
		
	//fahrkartenbestellungsErfassung
	public static double fahrkartenbestellungErfassen(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		double Fahrkartenanzahl = myScanner.nextDouble();
		System.out.print("Zu zahlender Betrag (in � / pro Fahrkarte): ");
		double Geldeingabe = myScanner.nextDouble();
		return  Geldeingabe * Fahrkartenanzahl;
		
	}	
	
	//FahrkartenBezahlen
	public static double zuZahlenderBetrag(String text2, double zuZahlenderBetrag) {
		Scanner myScanner = new Scanner(System.in);
		double eingeworfeneM�nze;		
		double eingezahlterGesamtbetrag = 0.00;	
		double r�ckgabebetrag;
		System.out.print(text2);
	       
		   while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("%4.2f",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.println(" Euro");
	    	   System.out.print("Geldeingabe (in �): ");
	    	   eingeworfeneM�nze = myScanner.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;   
	       }
	       
		   r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       
	       return r�ckgabebetrag;
	}
	
	//fahrkartenAusgeben
	public static void fahrkartenAusgeben () {
		
		   System.out.println("Fahrscheine "  + "werden ausgegeben:");
	       
	       
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		
	}
	
	//rueckgeldAusgeben
	public static void rueckgeldAusgeben (double r�ckgabebetrag) {
		
		if(r�ckgabebetrag > 0.00)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%4.2f", (r�ckgabebetrag));
	    	   System.out.println(" Euro");
	    	   System.out.println("wird in folgenden Scheinen/M�nzen ausgezahlt:");

	    	   while(r�ckgabebetrag >= 500.00) // 500 EURO-Schein
	           {
	        	  System.out.println("500 EURO");
		          r�ckgabebetrag -= 500.00;
	           }
	    	   while(r�ckgabebetrag >= 200.00) // 200 EURO-Schein
	           {
	        	  System.out.println("200 EURO");
		          r�ckgabebetrag -= 200.00;
	           }
	    	   while(r�ckgabebetrag >= 100.00) // 100 EURO-Schein
	           {
	        	  System.out.println("100 EURO");
		          r�ckgabebetrag -= 100.00;
	           }
	    	   while(r�ckgabebetrag >= 50.00) // 50 EURO-Schein
	           {
	        	  System.out.println("50 EURO");
		          r�ckgabebetrag -= 50.00;
	           }
	    	   while(r�ckgabebetrag >= 20.00) // 20 EURO-Schein
	           {
	        	  System.out.println("20 EURO");
		          r�ckgabebetrag -= 20.00;
	           }
	    	   while(r�ckgabebetrag >= 10.00) // 10 EURO-Schein
	           {
	        	  System.out.println("10 EURO");
		          r�ckgabebetrag -= 10.00;
	           }
	    	   while(r�ckgabebetrag >= 5.00) // 5 EURO-M�nzen
	           {
	        	  System.out.println("5 EURO");
		          r�ckgabebetrag -= 5.00;
	           }
	    	   while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.00;
	           }
	           while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.00;
	           }
	           while(r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.50;
	           }
	           while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.20;
	           }
	           while(r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.10;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	           
	           System.out.printf("\nVergessen Sie nicht, ihr Wechselgeld mitzunehmen und die Fahrscheine\n"+
	                   "vor Fahrtantritt entwerten zu lassen!\n"+
	                   "Wir w�nschen Ihnen eine gute Fahrt.");
	           
	           }
	}
}

