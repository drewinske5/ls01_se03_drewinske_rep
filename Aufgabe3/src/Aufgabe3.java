
public class Aufgabe3 {

	public static void main(String[] args) {
		int a    = -20;
		int b    = -10;
		int c    =   0;
		int d    =  20;
		int e    =  30;
		double f = -28.8889;
		double g = -23.3333;
		double h = -17.7778;
		double i =  -6.6667;
		double j =  -1.1111;
		
		System.out.println("--------AUFGABE 3------");
		System.out.println(                         );                    //Leer Zeile
		System.out.printf ("%-12s|%10s%n" , "Fahrenheit","Celsius");
		System.out.println("-----------------------");
		System.out.printf ("%+-12d|%10.2f%n" , a,f);
		System.out.printf ("%+-12d|%10.2f%n" , b,g);
		System.out.printf ("%+-12d|%10.2f%n" , c,h);
		System.out.printf ("%+-12d|%10.2f%n" , d,i);
		System.out.printf ("%+-12d|%10.2f%n" , e,j);


	}

}
