﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    
       //Fahrkartenautomat alt (Ohne Methoden)
       //Fahrkartenautomat alt (Ohne Methoden)
       //Fahrkartenautomat alt (Ohne Methoden)
       //Fahrkartenautomat alt (Ohne Methoden)
       
       Scanner tastatur = new Scanner(System.in);
       
       //Zuweisungen
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int anzahlFahrkarten;
       int fahrkartenAusgabe;
       int repeater = 1;
		
       do{
       
       // Fahrkartenanzahl
       // ----------------
       System.out.printf("Anzahl Fahrkarten: ");
       anzahlFahrkarten = tastatur.nextInt();
       
       // Geldeingabe
       // ----------------
       System.out.printf("Zu zahlender Betrag (in € / pro Fahrkarte): ");
       zuZahlenderBetrag = tastatur.nextDouble() * anzahlFahrkarten;
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.00;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: " + "%4.2f",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.println(" Euro");
    	   System.out.print("Geldeingabe (in €): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgabe = 2;
       if(anzahlFahrkarten < fahrkartenAusgabe)
       {
    	   System.out.println( "(" + (anzahlFahrkarten) + ")" + " Fahrschein"  + " wird ausgegeben:");
       }
       else
       {	
    	   System.out.println( "(" + (anzahlFahrkarten) + ")" + " Fahrscheine "  + " werden ausgegeben:");
       }
       
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	   
       if(rückgabebetrag > 0.00)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%4.2f" , (rückgabebetrag));
    	   System.out.println(" Euro");
    	   System.out.println("wird in folgenden Scheinen/Münzen ausgezahlt:");

    	   while(rückgabebetrag >= 500.00) // 500 EURO-Schein
           {
        	  System.out.println("500 EURO");
	          rückgabebetrag -= 500.00;
           }
    	   while(rückgabebetrag >= 200.00) // 200 EURO-Schein
           {
        	  System.out.println("200 EURO");
	          rückgabebetrag -= 200.00;
           }
    	   while(rückgabebetrag >= 100.00) // 100 EURO-Schein
           {
        	  System.out.println("100 EURO");
	          rückgabebetrag -= 100.00;
           }
    	   while(rückgabebetrag >= 50.00) // 50 EURO-Schein
           {
        	  System.out.println("50 EURO");
	          rückgabebetrag -= 50.00;
           }
    	   while(rückgabebetrag >= 20.00) // 20 EURO-Schein
           {
        	  System.out.println("20 EURO");
	          rückgabebetrag -= 20.00;
           }
    	   while(rückgabebetrag >= 10.00) // 10 EURO-Schein
           {
        	  System.out.println("10 EURO");
	          rückgabebetrag -= 10.00;
           }
    	   while(rückgabebetrag >= 5.00) // 5 EURO-Münzen
           {
        	  System.out.println("5 EURO");
	          rückgabebetrag -= 5.00;
           }
    	   while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.00;
           }
           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.00;
           }
           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.50;
           }
           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.20;
           }
           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.10;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       if(anzahlFahrkarten < fahrkartenAusgabe)
       {
    	   System.out.printf("\nVergessen Sie nicht, ihr Wechselgeld mitzunehmen und den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir wünschen Ihnen eine gute Fahrt.");
       }
       else
       {	
    	   System.out.printf("\nVergessen Sie nicht, ihr Wechselgeld mitzunehmen und die Fahrscheine\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir wünschen Ihnen eine gute Fahrt.");
       }
       
       System.out.println("\n\n");
		for (int i = 0; i < 25; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		
       }while(repeater < 2);
       
    }
}